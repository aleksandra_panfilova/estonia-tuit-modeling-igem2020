function y = cellN(x,p)
y = (x.*p.V)./p.cell_dmass;
end