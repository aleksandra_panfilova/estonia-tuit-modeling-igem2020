% Constitutive gene expression model.
% Updated 17/06/2020 Alejandro Vignoni
% x is in format [Prom.TF, mRNA, Protein, Biomass, Glucose, Ethanol, DAG]
function [dxdt] = model_EL222_v2(t,x,p,I)

heavy =@(t, period, duty, shift) double( mod( t - shift , period ) < duty * period );

Ug=(p.U_maxg*x(7))/(p.Ksg+x(7)+p.Age*x(8));
Ue=(p.U_maxe*x(8))/(p.Kse+x(8)+p.Aeg*x(7));

%x3 = Biomass, g/L
dxdt(6,1)=(Ug+Ue)*x(6);

%x4 = Glucose, g/L
dxdt(7,1)=-(Ug*x(6))/p.yxg;

%x5 = Ethanol, g/L
dxdt(8,1)=p.ke*Ug*x(6)-(Ue*x(6))/p.yxe;

%I_step = I;
I_step = heaviside(t-25)*I;
%I_step = heavy(t, 10, 0.5, 5)*I;

%x1 = Promoter.TF
dxdt(1,1) = +I_step*p.kon*(p.TF_v-x(1))-p.koff*x(1);

%x2 = PAP mRNA
dxdt(2,1) = p.kbasal_pah+p.kmax_pah*(x(1)^p.n_pah/(p.kd_pah^p.n_pah+x(1)^p.n_pah))-p.d1*x(2);

%x3 = PAP, molecules/cell
dxdt(3,1)=p.k2*x(2)-p.d2_pah*x(3);
   
%x4 = DGAT mRNA
dxdt(4,1) = p.kbasal_dgat+p.kmax_dgat*(x(1)^p.n_dgat/(p.kd_dgat^p.n_pah+x(1)^p.n_dgat))-p.d1*x(4);

%x5 = DGAT, molecules/cell
dxdt(5,1)=p.k2*x(4)-p.d2_dgat*x(5);

% PAP, molecules/L
pap = ((x(3)+2607)*cellN(x(6),p))/p.V;

% DGAT, molecules/L
dgat = ((x(5)+1431)*cellN(x(6),p))/p.V;

% microsomal PA, umol/L
PA = (24.2*x(6)*1000)/p.mr_pa;

%x3 = DAG
dxdt(10,1)=(((pap/p.Na)*1000)*p.kcat_pah*PA)/(p.km_pah+PA); % mmol L-1 h-1

%x3 = TAG
dxdt(11,1)=(((dgat/p.Na)*1000)*p.kcat_dgat*x(10))/((p.km_dgat/1000)+x(10)); % mmol L-1 h-1

end