% Time course of X for the C1FFL simulation
% X is switched on at t=1 and off at t=1.5;
% then again at t=5 and t=10

function x=X(time)
    x = 1*(time>25).*(time<72); %+*(time>5).*(time<10);
end