To run the simulation, save all .m files to one folder and run Main_TF_vector.m. 
You will get the dinamics of PAP and DGAT accumulation (Figure 1) and TAG production along with biomass accumulation (Figure 2).
x0 vector will contain all numerical values for the simulated processes.
More detailed comments are provided in the scripts.