p.mr_dgat = 55278;      % Da, https://www.genecards.org/cgi-bin/carddisp.pl?gene=DGAT1
p.V = 0.02;                % liquid culture volume, L
p.mr_pa = 770.046348;       % PA Mr, calculations are in PA_molecular_weight_calc
p.mr_pah = 95031;       % Da, https://www.uniprot.org/uniprot/P32567
p.mr_dag = 594.9;       % g/mol,
p.mr_tag = 992.079522;
p.Na = 6.02e23;       % Avogadro number


p.d1 = 0.042116*60;    % mRNA degradation rate  [1/min]
p.d2_pah = (log(2)/22)*60;       % degradation rate [1/min]
p.k2 = 1.4514*60;      % translation rate  [1/min] 5.5AAs per second in yeast
p.k1 = p.Na*0.0198*60;      % transcription rate [1/min] 0.0198 mol/min
p.kbasal_pah = 0.02612*60; % basal transcription rate
p.kmax_pah = 13.588*60;    % maximal induced transcription rate 

p.n_pah = 4.203;        % hill coefficient
p.n_dgat = 4.6403;

p.kd_pah = 956.75;      % level required for achieving kmax/2
p.kd_dgat = 1462.5;

p.TF_v = 2000;      % The number of TF molecules
p.cell_dmass = 47.65e-12; % mass of a dry yeast cell in g


%Michaelis and Menten's model parameters
p.km_pah = 50;          % uM
p.km_dgat = 15.9;        % uM, human, https://www.nature.com/articles/s41586-020-2280-2/tables/2?draft=collection

p.kcat_pah = 2.7*10^3*60;  % hr-1 at ph7 and 30C
p.kcat_dgat = ((1643.4*5000)/((5/p.mr_dgat)*1e+9))*60;      % hr-1, https://www.uniprot.org/uniprot/P9WQP3

%DGAT1 B. napus, under 2xBS-C180pr



p.d2_dgat = (log(2)/18)*60;  % degradation rate [1/min], https://www.jbc.org/content/early/2002/10/28/jbc.M207353200.full.pdf
p.kbasal_dgat = 0;      % inferred for GAL-5xBS-GAL
p.kmax_dgat = 10.22731087*60;   % inferred GAL-5xBS-GAL

%Specific growth rates on glucose and ethanol
p.U_maxg = 0.4048;
p.U_maxe = 0.0595;
p.Ksg = 0.4137;
p.Kse = 0.5618;
p.Age = 1.0636;
p.Aeg = 1.2964;
p.yxg = 0.1669;
p.yxe = 0.680952381;
p.ke = 2.8;          % k1 is a parameter representing the ethanol formation during glucose consumption
p.a1 = 0.7545;
p.a2 = 13.9280;
p.beta = 0.2804;
p.mr = 536.8726; %gmol-1 beta carotene molar mass



p.kon = 0.0016399*60;       % Binding rate  [1/min]
p.koff = 0.34393*60;    % Unbinding rate [1/min]



colorm = flip(summer);
figure

tfin = 72;   %simulation final time
step = 0.01;    %simulation step
tspan = 0:step:tfin-step;
% options for ode function
opti = odeset('AbsTol',1e-8,'RelTol',1e-6);
Init =  [0 0 0 0 0 0.03938 10 0.095 0 0 0];  %initial conditions

[t0,x0] = ode23t(@(t,x) model_EL222_v2(t,x,p,420),tspan, Init, opti);


x0(:,3) = x0(:,3)+2607;
x0(:,5) = x0(:,5)+1431;

x0(:,11) = (x0(:,11).*p.mr_tag)./x0(:,6);

%subplot(2,1,1);
yyaxis left
plot(t0, x0(:,3),'-','LineWidth',2,'Color',[0, 0.4470, 0.7410]);
hold on
plot(t0, x0(:,5),'-','LineWidth',2,'Color',[0.9290, 0.6940, 0.1250]);
ylabel({'Protein concentration, molecules/cell'});
hold on
yyaxis right
a = area(t0, X(t0));
a.FaceAlpha = 0.2;
ylabel({'Light intensity, \muW cm^{-2}'});
xlabel('Time, hr')
grid on
legend('PAP','DGAT','Light program')
xlim([0 72])


hold on
grid on
  
b=1;

figure
subplot(2,1,1);
yyaxis left
plot(t0, x0(:,11),'-','LineWidth',2);
ylabel({'TAG concentration, mg/gCDW'});
hold on
yyaxis right
x0(:,13) = 420.*(t0>25);
a = area(t0, 420.*(t0>25));
a.FaceAlpha = 0.2;
%text(5, 10000, 'I=600uW/cm^2')

ylabel({'Light intensity, uW cm^{-2}'});
xlabel('Time (hr)')
grid on
hold off
xlim([0 72])

x0(:,12)=PA(x0(:,6));

subplot(2,1,2);
yyaxis left
plot(t0, x0(:,12),'LineWidth',2);
ylabel({'PA concentration, mg/L'});
xlabel('Time, hr')
hold on
yyaxis right
plot(t0, (x0(:,6)),'LineWidth',2);
plot(t0, (x0(:,7)),'LineWidth',2);
xlabel('Time (hr)')
legend('PA','Biomass', 'Glucose')
hold off
grid on
ylabel({'Biomass, glucose concentration, g/L'});
xlim([0 72])

out = MLGetFigure(.8,.8);

%figure_width = 7;
%figure_height = 5;
%set(gcf,'units','centimeters');
%set(gcf,'papersize',[figure_width figure_height]);
%set(gcf,'paperposition',[0,0,figure_width,figure_height]);
%print -dpng -painters -r300 'TF_induc.png';
%print -dpdf -painters -r300 'TF_induc.pdf'; %djpg